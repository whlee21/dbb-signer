/**
 * View Models used by Spring MVC REST controllers.
 */
package dbblabs.signer.web.rest.vm;
