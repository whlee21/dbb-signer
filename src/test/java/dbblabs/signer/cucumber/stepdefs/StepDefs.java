package dbblabs.signer.cucumber.stepdefs;

import dbblabs.signer.SignerApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = SignerApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
